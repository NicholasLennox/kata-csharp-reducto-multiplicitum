﻿using System;
using System.Linq;

namespace Kata_ReductoMultiplicitum
{
    class Program
    {
        // Just doing the Kata requirements as straight forwards as possible
        // No classes, no tests. That can be refactored later
        static void Main(string[] args)
        {
            Console.WriteLine("7 -> " + SumDigProd(9,8).ToString());
            Console.WriteLine("6 -> " + SumDigProd(16, 28).ToString());
            Console.WriteLine("2 -> " + SumDigProd(1, 2, 3, 4, 5, 6));
            Console.WriteLine("6 -> " + SumDigProd(26, 497, 62, 841));
            Console.WriteLine("8 -> " + SumDigProd(123, -99));
            Console.WriteLine("8 -> " + SumDigProd(167, 167, 167, 167, 167, 3));
        }

        // Take in any number of integer arguements
        public static int SumDigProd(params int[] args)
        {
            // Add all the individual elements together
            int sum = 0;
            foreach(int number in args)
            {
                sum += number;
            }
            // Now need to keep multiplying together until it is 1 digit
            return MultiplyUntilSingle(sum);
        }

        private static int MultiplyUntilSingle(int number)
        {
            // Need to create an array of digits, many ways to do this.

            // Convert to string so we can go through one char at a time
            string stringNumber = number.ToString();
            // Convert to a character array
            char[] arrayNumber = stringNumber.ToCharArray();
            // Convert the char[] to int[]
            int[] intArrayNumber = Array.ConvertAll(arrayNumber, c => (int)Char.GetNumericValue(c));
            // This can be done many ways - some more efficient (https://sung.codes/blog/2017/03/11/parsing-number-digits/)

            // Next step is to multiply all the individual digits together (this is where you saw Aggregate in the other solutions)
            int product = 1;
            foreach (int num in intArrayNumber)
            {
                product *= num;
            }
            // Need to check if this is one digit -> -9 to 9
            // Could do length conversions, but we can just check
            if(product > -10 && product < 10)
            {
                // We can exit
                return product;
            }
            // If we get here, its too big, call the function again to reduce
            return MultiplyUntilSingle(product);
        }
    }
}
